#!/usr/bin/env bash

_CURDIR=$(git rev-parse --show-toplevel 2>/dev/null | sed -e 's/(//')

_lang=go
[[ -n $_LANG ]] && _lang="$_LANG"


declare -a _case
[[ -n $_CASE ]] && \
    _case=("$_CASE") || \
    _case=(
        "${_CURDIR}/testdata/deprecated"
        "${_CURDIR}/testdata/with_gw_validate"
        "${_CURDIR}/testdata/with_self_import"
        "${_CURDIR}/testdata/with_validate"
        "${_CURDIR}/testdata/simple"
        "${_CURDIR}/testdata/with_gw"
    )


echo lang -- $_lang
echo cases -- "${_case[@]}"

for case in "${_case[@]}"
do
    protoc \
		-I . \
		-I "${_CURDIR}"/testdata/include \
		-I "${case}" \
        --go_out=":." \
        $(find ${case} -type f -iname \*.proto)
    protoc \
		-I . \
		-I "${_CURDIR}"/testdata/include \
		-I "${case}" \
		--plugin="${_CURDIR}"/build/protoc-gen-domain \
		--domain_out="lang=${_lang}:." \
		$(find ${case} -type f -iname \*.proto)
done

		# --domain_out="lang=${_lang},go_construct,go_getters,go_decorate_method,go_example:." \
