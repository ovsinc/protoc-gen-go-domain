_CURDIR := `git rev-parse --show-toplevel 2>/dev/null | sed -e 's/(//'`

linter := golangci-lint
test_run := go test -run=^Example


MAJORVERSION := 0
MINORVERSION := 2
PATCHVERSION := 6
VERSION ?= ${MAJORVERSION}.${MINORVERSION}.${PATCHVERSION}


BIN := "${_CURDIR}/build/protoc-gen-domain"
GO ?= go
FLAGS ?= -trimpath -mod=readonly -modcacherw
LDFLAGS ?= -w -s -X "gitlab.com/ovsinc/protoc-gen-domain/internal/config.PgdVersion=${VERSION}"


.PHONY:
all: test lint build


.PHONY:
lint: go_lint  ## Full liner checks

.PHONY:
go_lint: ## Lint the files
	@${linter} run



.PHONY:
tests: go_test go_msan go_race ## Run unittests


.PHONY:
go_test: ## Run unittests
	@${test_run} -short ${PKG_LIST}

.PHONY:
go_race: ## Run data race detector
	@${test_run} -race ${PKG_LIST}


.PHONY:
go_msan: ## Run memory sanitizer
	@CXX=clang++ CC=clang \
	${test_run} -msan ${PKG_LIST}


.PHONY:
clean: ## Clean all
	@go clean -cache
	@find ${_CURDIR}/testdata -type f -name \*.go -exec rm -f '{}' \;
	@rm -f $(BIN)


lang ?=
case ?=

.PHONY:
test_with_data: build ## Build testdata case
	@_CASE=${case} _LANG=${lang} ${_CURDIR}/scripts/build_test.sh


.PHONY:
debug_build: ## Build debug out
	@protoc \
		-I . \
		-I ${_CURDIR}/testdata/include \
		-I ${_CURDIR}/testdata/$(case) \
		--debug_out="${_CURDIR}/testdata/$(case)/:." \
		${_CURDIR}/testdata/$(case)/*.proto


.PHONY:
bench: ## Run benchmark tests
	@${test_run} -benchmem -run=^# -bench=. ${_CURDIR}

.PHONY:
coverage: ## Generate global code coverage report
	@go test -cover ${_CURDIR}

.PHONY:
coverhtml: ## Generate global code coverage report in HTML
	[ -x /opt/tools/bin/coverage.sh ] && /opt/tools/bin/coverage.sh html || \
	${_CURDIR}/scripts/tools/coverage.sh html ${_CURDIR}/build/coverage.html

.PHONY:
dep: ## Get the dependencies
	@go mod vendor

.PHONY: build
build: ## Build the binary
	@$(GO) build $(FLAGS) -ldflags '$(LDFLAGS)' -o $(BIN)

.PHONY:
build_force: ## Build the binary force
	@$(GO) build -a $(FLAGS) -ldflags '$(LDFLAGS)' -o $(BIN)



.PHONY:
help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
