package main

import (
	pgs "github.com/lyft/protoc-gen-star/v2"
	pgsgo "github.com/lyft/protoc-gen-star/v2/lang/go"
	"gitlab.com/ovsinc/protoc-gen-domain/internal/templates/golang"
)

func main() {
	pgs.Init(
		pgs.DebugEnv("DEBUG"),
	).RegisterModule(
		NewDomainModule(),
	).RegisterPostProcessor(
		pgsgo.GoFmt(),
		golang.GoImports(),
		golang.GoFumpt(),
	).Render()
}
