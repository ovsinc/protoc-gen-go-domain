module gitlab.com/ovsinc/protoc-gen-domain

go 1.18

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/lyft/protoc-gen-star/v2 v2.0.3
	golang.org/x/tools v0.13.0
	mvdan.cc/gofumpt v0.5.0
)

require (
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/spf13/afero v1.6.0 // indirect
	golang.org/x/mod v0.12.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/protobuf v1.23.0 // indirect
)
