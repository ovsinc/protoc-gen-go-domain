package main

import (
	"text/template"

	"github.com/davecgh/go-spew/spew"
	pgs "github.com/lyft/protoc-gen-star/v2"
	pgsgo "github.com/lyft/protoc-gen-star/v2/lang/go"
	"gitlab.com/ovsinc/protoc-gen-domain/internal/templates/golang"
	"gitlab.com/ovsinc/protoc-gen-domain/pkg/module"
	"gitlab.com/ovsinc/protoc-gen-domain/pkg/utils"
)

type DomainModule struct {
	*pgs.ModuleBase

	ctx        pgsgo.Context
	langModule module.DomainModuler
	tmpl       *template.Template
}

func NewDomainModule() *DomainModule {
	return &DomainModule{ModuleBase: &pgs.ModuleBase{}}
}

func (p *DomainModule) generate(f pgs.File) {
	if len(f.AllMessages()) == 0 &&
		len(f.Services()) == 0 {
		p.Debugf("file '%s' - no services: nothing to do", f.Name())
		return
	}

	p.Debugf("processing file '%s'", f.Name())

	name := p.ctx.
		OutputPath(f).
		SetExt(p.langModule.Ext())

	p.AddGeneratorTemplateFile(name.String(), p.tmpl, f)
}

func (p *DomainModule) Execute(targets map[string]pgs.File, pkgs map[string]pgs.Package) []pgs.Artifact {
	p.Debugf("execute targets %+v", targets)
	p.Debugf("execute pkgs %+v", pkgs)

	for _, t := range targets {
		p.generate(t)
	}
	return p.Artifacts()
}

func (p *DomainModule) InitContext(c pgs.BuildContext) {
	p.ModuleBase.InitContext(c)
	p.ctx = pgsgo.InitContext(c.Parameters())

	lang := utils.ParseLang(c.Parameters().StrDefault(utils.LangPARAM, utils.LangDefVal))

	switch lang {
	case utils.LangGO:
		p.langModule = golang.NewDomainGoModule(p.ctx)
	case utils.LangUNKNOWN:
		panic(utils.ErrUnknownLang)
	default:
		panic(utils.ErrUnknownLang)
	}

	p.Debugf("generate for %s lang", lang)
	p.Debugf("parameters %s", spew.Sdump(c.Parameters()))

	p.tmpl = template.Must(p.langModule.ParseTemplate())

	p.Debugf("parse template done")
}

func (p *DomainModule) Name() string {
	return "domain"
}
