package golang

import (
	"strings"

	pgs "github.com/lyft/protoc-gen-star/v2"
	fumpt "mvdan.cc/gofumpt/format"
)

type goFumpt struct{}

// GoImports returns a PostProcessor that run goimports on any files ending . ".go"
func GoFumpt() pgs.PostProcessor { return goFumpt{} }

func (g goFumpt) Match(a pgs.Artifact) bool {
	var n string

	switch t := a.(type) {
	case pgs.GeneratorFile:
		n = t.Name
	case pgs.GeneratorTemplateFile:
		n = t.Name
	case pgs.CustomFile:
		n = t.Name
	case pgs.CustomTemplateFile:
		n = t.Name
	default:
		return false
	}

	return strings.HasSuffix(n, ".go")
}

func (g goFumpt) Process(in []byte) ([]byte, error) {
	// We do not want to give a filename here, ever.
	return fumpt.Source(in, fumpt.Options{})
}

var _ pgs.PostProcessor = goImports{}
