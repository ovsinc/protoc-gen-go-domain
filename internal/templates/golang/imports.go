package golang

import (
	"strings"

	pgs "github.com/lyft/protoc-gen-star/v2"
	"golang.org/x/tools/imports"
)

type goImports struct{}

// GoImports returns a PostProcessor that run goimports on any files ending . ".go"
func GoImports() pgs.PostProcessor { return goImports{} }

func (g goImports) Match(a pgs.Artifact) bool {
	var n string

	switch t := a.(type) {
	case pgs.GeneratorFile:
		n = t.Name
	case pgs.GeneratorTemplateFile:
		n = t.Name
	case pgs.CustomFile:
		n = t.Name
	case pgs.CustomTemplateFile:
		n = t.Name
	default:
		return false
	}

	return strings.HasSuffix(n, ".go")
}

func (g goImports) Process(in []byte) ([]byte, error) {
	// We do not want to give a filename here, ever.
	return imports.Process("", in, nil)
}

var _ pgs.PostProcessor = goImports{}
