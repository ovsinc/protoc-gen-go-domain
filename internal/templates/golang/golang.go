package golang

import (
	"bufio"
	"strings"
	"text/template"

	pgs "github.com/lyft/protoc-gen-star/v2"
	pgsgo "github.com/lyft/protoc-gen-star/v2/lang/go"
	"gitlab.com/ovsinc/protoc-gen-domain/internal/config"
	"gitlab.com/ovsinc/protoc-gen-domain/pkg/module"
	"gitlab.com/ovsinc/protoc-gen-domain/pkg/utils"
)

const (
	GO_STRUCT_CONSTRUCT_PARAM = "go_construct"       //nolint:revive,stylecheck // like a ENV
	GO_STRUCT_GETTERS_PARAM   = "go_getters"         //nolint:revive,stylecheck // like a ENV
	GO_DECORATE_METHOD        = "go_decorate_method" //nolint:revive,stylecheck // like a ENV
	GO_EXAMPLE                = "go_example"         //nolint:revive,stylecheck // like a ENV
)

const deprecatedStr = "// Deprecated: Do not use.\n"

func NewDomainGoModule(ctx pgsgo.Context) module.DomainModuler {
	structContruct, _ := ctx.Params().BoolDefault(GO_STRUCT_CONSTRUCT_PARAM, false)
	getters, _ := ctx.Params().BoolDefault(GO_STRUCT_GETTERS_PARAM, false)
	decorators, _ := ctx.Params().BoolDefault(GO_DECORATE_METHOD, false)
	example, _ := ctx.Params().BoolDefault(GO_EXAMPLE, false)

	dgm := domainGoModule{
		ctx:  ctx,
		lang: utils.LangGO,
	}

	dgm.tmpl = template.New("domain").Funcs(map[string]interface{}{
		"gopackage": dgm.gopackageFn,
		"goname":    dgm.gonameFn,
		"gotype":    dgm.gotypeFn,
		"gocomment": dgm.gocommentFn,

		"deprecated_file":  dgm.deprecatedFileFn,
		"unescapeHTML":     utils.UnescapeHTML,
		"goname_with_base": dgm.gonameWithBaseFn,
		"goparam_name":     dgm.goParamNameFn,

		"conf": func() GoTemplConf {
			return GoTemplConf{
				PgdVersion: config.PgdVersion,
				PgdName:    config.PgdName,
				Contructor: structContruct,
				Getters:    getters,
				Decorators: decorators,
				Example:    example,
			}
		},
	})

	return &dgm
}

type domainGoModule struct {
	ctx  pgsgo.Context
	lang utils.Lang
	tmpl *template.Template
}

func (dgm *domainGoModule) Ext() string {
	return dgm.lang.Ext()
}

func (dgm *domainGoModule) ParseTemplate() (*template.Template, error) {
	return dgm.tmpl.Parse(goDomainTpl)
}

// go template funcs

func (dgm *domainGoModule) gonameFn(node pgs.Node) string {
	return goName(dgm.ctx, node).String()
}

func (dgm *domainGoModule) goParamNameFn(base, node interface{ Name() pgs.Name }) string {
	return strings.ToLower(
		base.Name().String() + "_" + node.Name().String())
}

func (dgm *domainGoModule) gonameWithBaseFn(subs ...pgs.Node) string {
	if len(subs) == 0 {
		return ""
	}

	str := dgm.gonameFn(subs[0])
	for _, s := range subs[1:] {
		str += "_"
		str += dgm.gonameFn(s)
	}

	return str
}

func (dgm *domainGoModule) gotypeFn(node pgs.Node) string {
	if f, ok := node.(pgs.Field); ok {
		return dgm.ctx.Type(f).String()
	}
	return dgm.gonameFn(node)
}

func (dgm *domainGoModule) deprecatedFileFn(node pgs.File) string {
	if node.Descriptor().GetOptions().GetDeprecated() {
		return deprecatedStr
	}
	return ""
}

func (dgm *domainGoModule) deprecatedStr(node pgs.Node) string {
	deprecatedStrSolver := func(b bool) string {
		if b {
			return deprecatedStr
		}
		return ""
	}

	switch en := node.(type) {
	case pgs.File:
		return deprecatedStrSolver(en.Descriptor().GetOptions().GetDeprecated())
	case pgs.Message:
		return deprecatedStrSolver(en.Descriptor().GetOptions().GetDeprecated())
	case pgs.Enum:
		return deprecatedStrSolver(en.Descriptor().GetOptions().GetDeprecated())
	case pgs.EnumValue:
		return deprecatedStrSolver(en.Descriptor().GetOptions().GetDeprecated())
	case pgs.Service:
		return deprecatedStrSolver(en.Descriptor().GetOptions().GetDeprecated())
	case pgs.Method:
		return deprecatedStrSolver(en.Descriptor().GetOptions().GetDeprecated())
	case pgs.Field:
		return deprecatedStrSolver(en.Descriptor().GetOptions().GetDeprecated())
	}

	return ""
}

func (dgm *domainGoModule) gopackageFn(node pgs.Node) string {
	return dgm.ctx.PackageName(node).String()
}

func (dgm *domainGoModule) gocommentFn(node pgs.Entity) string {
	out := dgm.deprecatedStr(node)
	sc := bufio.NewScanner(strings.NewReader(node.SourceCodeInfo().LeadingComments()))
	for sc.Scan() {
		out = out + "// " + strings.TrimSpace(sc.Text()) + "\n"
	}
	return out
}
