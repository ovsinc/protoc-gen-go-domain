package utils

import (
	"html/template"
)

func UnescapeHTML(s string) template.HTML {
	return template.HTML(s) //nolint: gosec // this is intentional
}
