package utils

import (
	"errors"
	"strings"
)

const (
	LangPARAM  = "lang"
	LangDefVal = "go"
)

type Lang int32

const (
	LangUNKNOWN Lang = iota
	LangGO
	// Lang_CSHARP
)

var ErrUnknownLang = errors.New("unsupported language")

func ParseLang(s string) Lang {
	l := LangUNKNOWN //nolint:ineffassign // i known to do
	switch strings.ToLower(s) {
	case "go", "golang":
		l = LangGO
	// case "c#", "cs", "csharp":
	// 	l = Lang_CSHARP
	default:
		l = LangUNKNOWN
	}
	return l
}

func (l Lang) String() string {
	s := "unknown" //nolint:goconst,ineffassign // i known to do
	switch l {
	case LangGO:
		s = "go"
	// case Lang_CSHARP:
	// 	s = "c#"
	case LangUNKNOWN:
		s = "unknown"
	default:
		s = "unknown"
	}
	return s
}

func (l Lang) Ext() string {
	ext := ""
	switch l {
	case LangGO:
		ext = "_domain.go"
	// case Lang_CSHARP:
	// 	ext = "_domain.cs"
	case LangUNKNOWN:
		ext = ""
	default:
		ext = ""
	}
	return ext
}
