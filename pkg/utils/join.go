package utils

import (
	"fmt"
	"unicode"
	"unicode/utf8"

	pgs "github.com/lyft/protoc-gen-star/v2"
	pgsgo "github.com/lyft/protoc-gen-star/v2/lang/go"
)

func JoinChild(a, b pgs.Name) pgs.Name {
	if r, _ := utf8.DecodeRuneInString(b.String()); unicode.IsLetter(r) && unicode.IsLower(r) {
		return pgs.Name(fmt.Sprintf("%s%s", a, pgsgo.PGGUpperCamelCase(b)))
	}
	return JoinNames(a, pgsgo.PGGUpperCamelCase(b))
}

func JoinNames(a, b pgs.Name) pgs.Name {
	return pgs.Name(fmt.Sprintf("%s_%s", a, b))
}
