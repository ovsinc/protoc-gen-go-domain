package module

import "text/template"

type DomainModuler interface {
	ParseTemplate() (*template.Template, error)
	Ext() string
}
